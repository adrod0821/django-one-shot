# from django.shortcuts import render
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from todos.models import TodoList

# Create your views here.


class TodoListListView(ListView):
    model = TodoList
    template_name = "todos/list.html"
    context_object_name = "todolist_name"


class TodoListDetailView(DetailView):
    model = TodoList
    template_name = "todos/details.html"
    context_object_name = "todolist"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context
